'use strict';
const express = require('express');
const timber = require('timber');
const bodyParser = require('body-parser');
const app = express();
var srv = require('http').createServer(app);

app.get('/', function (req, res, next) {
  res.send('You POSTed to the micro-service!');
});

app.post('/', function (req, res, next) {
  res.send('You POSTed to the micro-service!');
});

srv.listen(3000, function () {
  console.log('Listening on 3000');
});
